# $1 = BUILD IMAGE
# $2 = FROM IMAGE
# $3 = TAG
# $4 = PASSWORD
cat Dockerfile-template \
  | sed "s#%BUILD#$1#g" \
  | sed "s#%FROM#$2#g" \
  > Dockerfile
docker-build $4 $3
